
all: metad metac

obj-metad =  metad.o mainloop.o classes.o commands.o control.o daemon.o stream.o \
	read_config.o service.o version.o skip.o strlistdup.o error.o strsplit.o \
	broadcast.o ports.o sendlist.o loadstate.o recvlist.o 

obj-metac =  meta.o sendcmd.o broadcast.o dlink.o args.o version.o skip.o \
	ports.o recvlist.o prtime.o

CFLAGS=-O -Wall -Werror

metad : $(obj-metad)
	$(CC) -o $@ $(obj-metad)
metac : $(obj-metac)
	$(CC) -o $@ $(obj-metac)


clean :
	rm -f $(obj-metad) $(obj-metac) metac metad *.d

%.d: %
	@mkdir -p $(dir $@)
	@{ \
	  echo -n $<-dep' = ' ; \
	  sed -n -e 's,^#include.*"\(.*\)".*,\1 $$(\1-dep),p' $< | tr '\012' ' ' ; \
	  echo ; \
	  echo -n '-include ' ; \
	  sed -n -e 's,^#include.*"\(.*\)".*,\1.d,p' $< | tr '\012' ' ' ; \
	  echo ; \
	} | sed -e 's,[^ /]*/\.\./,,g' > $@

%.o : %.c
	$(CC) $(CFLAGS) -c $<

-include $(patsubst %.o,%.c.d,$(obj-metad))
-include $(patsubst %.o,%.c.d,$(obj-metac))

$(foreach t, $(sort $(obj-metad) $(obj-metac)),$(eval $t : $($(patsubst %o,%c,$(t))-dep)))


