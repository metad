
/*
 * args - a getopt link argument processor
 *
 * void *handle = args_init(argc, argv, "argctl");
 *
 * args_next(handle, &pos, &inv, &optarg) -> BAD_FLAG/END_ARGS/NO_FLAG/opt
 *
 * argctl = {<optchar><typechar>}*
 *  typechar ==  ?    -f  or -~f
 *               +    -f arg or -~f
 *               -    -f  or -~f arg
 *               :    -f  arg   only
 *
 *
 */

#define IN_ARGS
#include	"args.h"
void *malloc(int);
struct handle
{
	int argc;
	char **argv;
	char *ctl;
	int nextarg, nextchar;
};
#define NULLH ((struct handle *)0)

struct handle *args_init(int argc, char **argv, char *ctl)
{
	/* if ctl is bad, return NULL, else a handle */
	int i;
	struct handle *h;
	for (i=0 ; ctl[i]; i+= 2)
		if (ctl[i+1] != '?' && ctl[i+1] != '-' && ctl[i+1] != '+' && ctl[i+1] != ':')
			break;
	if (ctl[i])
		return NULLH;

	h = (struct handle*)malloc(sizeof(struct handle));
	if (h == NULLH)
		return h;
	h->argc = argc;
	h->argv = argv;
	h->ctl = ctl;
	h->nextarg = 1;
	h->nextchar = 0;
	return h;
}

int args_next(struct handle *h, int *pos, int *inv, char **opt)
{
	int invc = 0;
	int invfound = 0;
	int i;
	char *a;
	if (h->nextarg >= h->argc)
		return END_ARGS;
	if (h->nextchar == 0)
	{
		if (h->argv[h->nextarg][0] != '-')
		{
			if (pos) *pos = h->nextarg;
			if (inv) *inv = 0;
			if (opt) *opt = h->argv[h->nextarg];
			h->nextarg++;
			return NO_FLAG;
		}
		h->nextchar++;
	}
	a = h->argv[h->nextarg];
	while (a[h->nextchar] == '~')
	{
		invc = !invc;
		invfound = 1;
		h->nextchar++;
	}
	for (i=0 ; h->ctl[i] ; i+=2)
		if (h->ctl[i] == a[h->nextchar])
			break;
	if (h->ctl[i] == 0)
	{
		h->nextchar = 0;
		h->nextarg ++;
		return BAD_FLAG;
	}
	h->nextchar++;
	if (pos) *pos = h->nextarg;
	if (inv) *inv = invc;
	if (a[h->nextchar] == 0)
	{
		h->nextchar = 0;
		h->nextarg ++;
	}
	switch(h->ctl[i+1])
	{
	case '?':
		return h->ctl[i];
	case '+':
		if (inv)
			return h->ctl[i];
		break;
	case '-':
		if (!inv)
			return h->ctl[i];
		break;
	case ':':
		break;
	}
	/* need optarg */
	if (h->nextarg >= h->argc)
		return BAD_FLAG;
	if (opt)
		*opt = h->argv[h->nextarg] + h->nextchar;
	h->nextarg++;
	h->nextchar=0;
	return h->ctl[i];
}
