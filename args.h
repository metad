
#ifndef IN_ARGS
void *args_init(int argc, char **argv, char *ctl);
int   args_next(void *handle, int *pos, int *inv, char **opt);
#endif

#define	END_ARGS 0
#define BAD_FLAG (-1)
#define	NO_FLAG  (-2)
