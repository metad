
/* broadcast a string on all interfaces, to udp_port(); */

#include	<sys/types.h>
#include	<unistd.h>
#include	<sys/socket.h>
#include	<sys/ioctl.h>
#ifdef SOLARIS
#include	<sys/sockio.h>
#endif
#include	<netinet/in.h>
#include	<net/if.h>
#include	<fcntl.h>
#include	<netdb.h>
#include	<stdio.h>
#include	<memory.h>

int udp_port(void);

static struct ifconf	iflist;
static int interfaces;
static int sock;

static int ifconfinit()
{

	static char buf[2048];

	iflist.ifc_len = sizeof(buf);
	iflist.ifc_buf = buf;
/*	printf("ifc_len = %d\n", iflist.ifc_len);	/ **/
	if (ioctl(sock, SIOCGIFCONF, (char *)&iflist)< 0) return -1;
/*	printf("ifc_len = %d\n", iflist.ifc_len);	/ **/
	interfaces = iflist.ifc_len / sizeof(struct ifreq);
#if 0
/*	printf("interfaces = %d\n",interfaces);		/ **/
/*	for (in=0; in<interfaces ; in++) if (iflist.ifc_req[in].ifr_name[0]=='\0') break; */
/*	interfaces = in; */
/*	printf("interfaces = %d\n",interfaces);		/ **/
#endif
	return 0;
}

static void sendaddr(struct sockaddr_in addr, char *packet)
{
	addr.sin_port = udp_port();
	addr.sin_family = AF_INET;

	sendto(sock, packet, strlen(packet), 0, (struct sockaddr *)&addr, sizeof(addr));
}

static void sendinter(int n, char *packet)
{
	struct ifreq ifr;

	ifr = iflist.ifc_req[n];
/*    printf("interface = %s\n", ifr.ifr_name); */
	ioctl(sock, SIOCGIFBRDADDR, &ifr);
	sendaddr(*(struct sockaddr_in*)&ifr.ifr_broadaddr, packet);
}

void broadcast(char *packet)
{

	int n;
	int port;
	struct sockaddr_in	myaddr;
	int a = -1;


	sock = socket(AF_INET, SOCK_DGRAM, 0);
	setsockopt(sock, SOL_SOCKET, SO_BROADCAST, (char*)&a, 4);
	memset(&myaddr, 0, sizeof(myaddr));
	myaddr.sin_family = AF_INET;
	if (geteuid()==0)
		port = 1023;
	else
		port = 10230;
	myaddr.sin_port = htons(port);
	while (bind(sock, (struct sockaddr *)&myaddr, sizeof(myaddr))== -1) myaddr.sin_port = htons(--port);
	if (ifconfinit()!= -1)		/* gets list of interfaces */
		for (n = 0 ; n < interfaces ; n++)
			sendinter(n, packet);
}
