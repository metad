
#include	"metad.h"

extern struct class daemon_class;
extern struct class stream_class;
static struct class *classes[] = {
	&daemon_class,
	&stream_class,
	NULL
};

class_t find_class(char *name)
{
	int c;
	for (c=0 ; classes[c] ; c++)
		if (strcasecmp(name, classes[c]->class)==0)
			return classes[c];
	return NULL;
}
