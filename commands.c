
#include	"metad.h"
#include	"skip.h"

/* the commands are handled here */

static char *gather_arg(char **args)
{
	char *line;
	int len = 0;
	int a;
	for (a=0 ; args[a] ; a++) len += strlen(args[a])+1;
	line = (char*)malloc(len+1);
	len = 0;
	for (a=0 ; args[a] ; a++)
	{
		strcpy(line+len, args[a]);
		len += strlen(args[a]);
		line[len++] = ' ';
	}
	if (len>0)
		line[len-1] = '\0';
	else
		line[len] = 0;
	return line;
}

static void do_version(char **args, char *host, void *con)
{
	char *rv;

	if (con) {
		rv = (char*)malloc(strlen(version)+2);
		strcpy(rv+1, version);
		rv[0] = 2;
		set_reply(con, rv, strlen(version)+2);
	}
}


static void do_broad(char **args, char *host, void *con)
{
	/* remaining args are packaged up and broadcast on all interfaces */
	char *line;
	line = gather_arg(args+1);
	logmsg(LOG_INFO, "Broadcast request from %s for %s", host, line);
	broadcast(line);
	free(line);
}

static void do_die(char **args, char *host, void *con)
{
	logmsg(LOG_WARNING, "Die request from %s", host);
	exit(1);
}

static void do_disable(char **args, char *host, void *con)
{
	service_t sv;
	if (args[1] == NULL)
		return_error(con, "No service given for disabling");
	else if ((sv=find_service(args[1]))==NULL)
		return_error(con, "Cannot find service %s to disable it", args[1]);
	else for ( ; sv ; sv=find_service(NULL))
		if (sv->enabled) {
			logmsg(LOG_INFO, "Disable request from %s for %s", host, sv->service);
			(sv->class->disable_service)(sv);
			sv->enabled = 0;
		}
}

static void do_enable(char **args, char *host, void *con)
{
	service_t sv;
	if (args[1] == NULL)
		return_error(con, "No service given for enabling");
	else if ((sv=find_service(args[1]))==NULL)
		return_error(con, "Cannot find service %s to enable it", args[1]);
	else for ( ; sv ; sv=find_service(NULL))
		if (!sv->enabled) {
			logmsg(LOG_INFO, "Enable request from %s for %s", host, sv->service);
			(sv->class->register_service)(sv);
			sv->enabled = 1;
		}
}


static void do_run(char **args, char *host, void *con)
{
	/* attempt to run service called args[1] with METAD_ARG set to remaining args and METAD_HOST set to host
	 */
	service_t sv;
	if (args[1] == NULL)
		return_error(con, "No service given to run");
	else if ((sv=find_service(args[1]))==NULL)
		return_error(con, "Cannot find service %s to run", args[1]);
	else {
		char *env[3];
		char *arg = gather_arg(args+2);
		env[0] = strcat(strcpy((char*)malloc(20+strlen(host)), "METAD_REASON=run:"), host);
		env[1] = strcat(strcpy((char*)malloc(11+strlen(arg)), "METAD_ARG="), arg);
		env[2] = NULL;
		for ( ; sv ; sv = find_service(NULL))
		{
			/* first clear all hold-times */
			proc_t *pp;
			for (pp=skip_first(sv->proc_list) ; pp ; pp=skip_next(pp))
				if ((*pp)->hold_time != 0)
					(*pp)->hold_time = 1;
			sv->next_hold = 2;
			logmsg(LOG_INFO,"starting %s for %s : arg = %s", sv->service, host, arg);
			new_proc(sv, env);
		}
		free(arg);
		free(env[1]);
		free(env[0]);
	}
}

static void do_kill(char **args, char *host, void *con)
{
	/* args[1] = signal
	 * args[2] = pid or service name
	 */
	int sig;
	pid_t pid;
	service_t sv;
	if (args[1] == NULL)
		return_error(con, "No signal number given for kill");
	else if ((sig=atoi(args[1]))<= 0)
		return_error(con, "Bad signal number given for kill: %s", args[1]);
	else if (args[2] == NULL)
		return_error(con, "No process id or service name given for kill");
	else if ((pid = atoi(args[2]))>0) {
		proc_t *pp = skip_search(allprocs, &pid);
		if (pp) {
			logmsg(LOG_INFO, "killing %s for %s", args[2], host);
			if ((*pp)->exit_time == 0)
				kill((*pp)->pid, sig);
			else if ((*pp)->it_forked > 1)
				kill((*pp)->it_forked, sig);
		} else
			return_error(con, "Cannot find process %s to kill", args[2]);
	} else if ((sv = find_service(args[2]))!= NULL) {
		for ( ; sv ; sv = find_service(NULL)) {
			proc_t *pp;
			for (pp = skip_first(sv->proc_list) ; pp ; pp = skip_next(pp))
				if ((*pp)->exit_time == 0 || (*pp)->it_forked) {
					logmsg(LOG_INFO,
					       "signalling %s:%d with %d for %s", sv->service,
					       (*pp)->exit_time?(*pp)->it_forked:(*pp)->pid,
					       sig, host);
					if ((*pp)->exit_time == 0)
						kill((*pp)->pid, sig);
					else if ((*pp)->it_forked > 1)
						kill((*pp)->it_forked, sig);
				}
		}
	}
	else
		return_error(con, "Cannot find service %s to kill", args[2]);
}


static void do_reread(char **args, char *host, void *con)
{
	char *errs = NULL;
	int old;
	logmsg(LOG_INFO, "Rereading config file for %s", host);
	old= errors_to(ERROR_STRING, &errs);
	if (read_config(services, NULL) != 0)
		return_error(con, "%s", errs);
	errors_to(old, NULL);
	if (errs) free(errs);
}

static void do_list(char **args, char *host, void *con)
{
	service_t sv, *svp;
	init_return();
	send_byte(3); /* listing */
	if (args[1] == NULL) {
		for (svp = skip_first(services) ; svp ; svp = skip_next(svp))
			send_service(*svp);

		send_byte(0); /* finished */
		do_send(con);
	}
	else if ((sv=find_service(args[1])) != NULL) {
		for ( ; sv ; sv = find_service(NULL))
			send_service(sv);
		send_byte(0);
		do_send(con);
	} else
		return_error(con, "Cannot find service %s to list", args[1]);
}

void do_restart(char **args, char *host, void *con)
{
	logmsg(LOG_INFO, "About to restart for %s", host);
	control_close();
	prepare_restart();
	restart();
	exit(0);
}

static struct commands cmds[] = {
	{ "list",	do_list},
	{ "version",	do_version},
	{ "broad",	do_broad},
	{ "die",	do_die},
	{ "disable",	do_disable},
	{ "enable",	do_enable},
	{ "kick",	do_run},
	{ "run",	do_run},
	{ "kill",	do_kill},
	{ "reread",	do_reread},
	{ "restart",	do_restart},
	{ NULL, NULL}
};

int do_command(char **args, char *host, void *con)
{
	int cmd = 0;
	for (cmd = 0; cmds[cmd].name ; cmd++)
		if (strcasecmp(cmds[cmd].name, args[0])==0)
			break;
	if (cmds[cmd].name) {
		(cmds[cmd].proc)(args, host, con);
		return 1;
	} else
		return 0;
}
