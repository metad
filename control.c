
#define IN_CONTROL
#include	<stdio.h>
#include	"metad.h"
#include	<sys/socket.h>
#include	<netinet/in.h>
#include	<fcntl.h>
#ifdef SOLARIS
#include	<sys/fcntl.h>
#else
#include	<sys/file.h>
#endif

#include	<netdb.h>
#include	<ctype.h>

/* control.c - handle the control ports
 * listen on udp port for packets with command to obey
 * listen on tcp port for connections.
 *   when get connection, listen for command, and possibly return data
 *
 */

/* only allow one active tcp connection for now,
 * but make sure it times out
 */

static int udp_sock;
static int tcp_listen;

static struct tcpcon {
	int	sock;
	char	buf[1024];	/*for incoming command */
	char	host[1024];	/* host connection is from */
	int	buflen;		/* how much has been read */
	char	*outbuf;	/* outgoing data */
	int	outlen;		/* size of outgoing data */
	int	outpos;		/* how much sent so far */
	time_t	connect_time;	/* when the connection was established */
} tcpcon;

void return_error(struct tcpcon *con, char *fmt, char *a, char *b, char *c)
{
	char buf[1024];
	char *rv;

	if (con) {
		sprintf(buf, fmt, a, b, c);
		sprintf(buf+strlen(buf), " (metad version %s)", version);
		rv = (char*)malloc(strlen(buf)+2);
		strcpy(rv+1, buf);
		rv[0]= 1;
		con->outbuf = rv;
		con->outlen = strlen(buf)+2;
		con->outpos = 0;
	}
}

static int address_ok(struct sockaddr_in *sa, char *host)
{
	struct hostent *he;
	int len;
	int a;
	static char *tail = ".cse.unsw.edu.au";

	if (ntohs(sa->sin_port) >= 1024 && geteuid() == 0)
		return 0;
	if (sa->sin_addr.s_addr == htonl(0x7f000001)) {
		strcpy(host, "localhost");
		return 1; /* localhost */
	}
	he = gethostbyaddr((char*)&sa->sin_addr, 4, AF_INET);
	if (he == NULL)
		return 0;
	strcpy(host, he->h_name);
	he = gethostbyname(host);
	if (he == NULL)
		return 0;
	for (a=0; he->h_addr_list[a] ; a++)
		if (memcmp(&sa->sin_addr, he->h_addr_list[a], 4)==0) {
			/* well, we have a believable name */

			len = strlen(host);
			if (len > strlen(tail) && strcasecmp(tail, host+len - strlen(tail))== 0)
				return 1;
			return 0;
		}
	return 0;
}

static void run_command(char *buf, char *host, struct tcpcon *con)
{
	char *cp;
	char **words, **wp;

	for (cp= buf; *cp ; cp++)
		if (*cp == '\r' || *cp == '\n')
			*cp = 0;

	wp = words = strsplit(buf, " ");
	if (isdigit(wp[0][0]))
		wp++; /* old gossip put a port number at the start for return info */
	if (!do_command(wp, host, con))
		/* possibly return error */
		if (con)
			return_error(con, "unknown command %s", wp[0], NULL, NULL);
}

void nodelay(int socket)
{
	int f;
	f = fcntl(socket, F_GETFL, 0);
	fcntl(socket, F_SETFL, f|O_NDELAY);
	fcntl(socket, F_SETFD, 1); /* set close-on-exec */
}

int control_init()
{
	udp_sock = socket(AF_INET, SOCK_DGRAM, 0);
	if (udp_sock >= 0) {
		struct sockaddr_in sa;
		memset(&sa, 0, sizeof(sa));
		sa.sin_family = AF_INET;
		sa.sin_port = udp_port();
		nodelay(udp_sock);
		if (bind(udp_sock, (struct sockaddr *)&sa, sizeof(sa)) != 0) {
			error("cannot bind udp port");
			return -1;
		}
	} else {
		error("cannot create udp socket");
		return -1;
	}
	tcp_listen = socket(AF_INET, SOCK_STREAM, 0);
	if (tcp_listen >= 0) {
		struct sockaddr_in sa;
		int i = 1;
		nodelay(tcp_listen);
		memset(&sa, 0, sizeof(sa));
		sa.sin_family = AF_INET;
		sa.sin_port = tcp_port();
		setsockopt(tcp_listen, SOL_SOCKET, SO_REUSEADDR, (char*)&i, 4);
		if (bind(tcp_listen, (struct sockaddr *)&sa, sizeof(sa)) != 0) {
			error("cannot bind tcp port");
			return -1;
		}
		listen(tcp_listen, 5);
	} else {
		error("Cannot create tcp socket");
		return -1;
	}
	tcpcon.sock = -1;
	return 0;
}

void control_close(void)
{
	close(tcp_listen);
	close(udp_sock);
	close(tcpcon.sock);
}

void check_control(void)
{
	/* first check udp */
	if (readyon(udp_sock)) {
		char buf[1024];
		char host[1024];
		int n;
		struct sockaddr_in sa;
		unsigned int salen = sizeof(sa);
		n = recvfrom(udp_sock, buf, sizeof(buf)-1, 0, (struct sockaddr *)&sa, &salen );
		if (n>0 && address_ok(&sa, host)) {
			buf[n] = 0;
			run_command(buf, host, NULL);
		}
	}
	listenon(udp_sock);

	/* then check tcpcon or tcp_listen */
	if (tcpcon.sock != -1) {
		time_t now;
		time(&now);
		if (tcpcon.connect_time + 120 < now) {
			/* just give up */
			close(tcpcon.sock);
			tcpcon.sock = -1;
			if (tcpcon.outbuf) free(tcpcon.outbuf);
			tcpcon.outbuf = NULL;
			listenon(tcp_listen);
		} else if (tcpcon.outbuf) {
			if (canwrite(tcpcon.sock) && tcpcon.outpos < tcpcon.outlen)
			{
				int l = tcpcon.outlen - tcpcon.outpos;
				if (l>1024) l = 1024;
				l = write(tcpcon.sock, tcpcon.outbuf+tcpcon.outpos, l);
				if (l< 0)
				{
					close(tcpcon.sock); tcpcon.sock = -1; free(tcpcon.outbuf);
					tcpcon.outbuf = NULL;
				}
				else
					tcpcon.outpos += l;
				if (tcpcon.outpos >= tcpcon.outlen)
				{
					close(tcpcon.sock); tcpcon.sock = -1; free(tcpcon.outbuf);
					tcpcon.outbuf = NULL;
				}
			}
			if (tcpcon.sock == -1)
				listenon(tcp_listen);
			else
				writeon(tcpcon.sock);
		} else { /* we are still reading a command */
			if (readyon(tcpcon.sock)) {
				int l = sizeof(tcpcon.buf) - tcpcon.buflen;
				l = read(tcpcon.sock, tcpcon.buf+tcpcon.buflen, l-1);
				if (l<0) {
					close(tcpcon.sock);
					tcpcon.sock = -1;
				} else {
					tcpcon.buf[l] = 0;
					if (l == 0 ||
					    strchr(tcpcon.buf, '\n') ||
					    strchr(tcpcon.buf, '\r') ||
					    strlen(tcpcon.buf) < l) {
						run_command(tcpcon.buf, tcpcon.host, &tcpcon);
						if (tcpcon.outbuf == NULL) {
							tcpcon.outbuf = malloc(1);
							tcpcon.outbuf[0] = 0;
							tcpcon.outlen = 1;
							tcpcon.outpos = 0;
						}
					}
				}
			}
			if (tcpcon.sock == -1)
				listenon(tcp_listen);
			else if (tcpcon.outbuf)
				writeon(tcpcon.sock);
			else
				listenon(tcpcon.sock);
		}
	} else {
		if (readyon(tcp_listen)) {
			struct sockaddr_in sa;
			unsigned int salen = sizeof(sa);
			tcpcon.buflen = 0;
			tcpcon.outbuf = NULL;
			tcpcon.sock = accept(tcp_listen, (struct sockaddr *)&sa, &salen);
			if (tcpcon.sock >= 0) {
				nodelay(tcpcon.sock);
				if (address_ok(&sa, tcpcon.host)) {
					time(&tcpcon.connect_time);
					listenon(tcpcon.sock);
					waituntil(tcpcon.connect_time+122);
				} else {
					close(tcpcon.sock);
					tcpcon.sock = -1;
				}
			}
		}
		if (tcpcon.sock < 0)
			listenon(tcp_listen);
	}
}

void set_reply(struct tcpcon *con, char *reply, int len)
{
	if (con) {
		con->outbuf = reply;
		con->outlen = len;
		con->outpos = 0;
	} else
		free(reply);
}
