
#include	"metad.h"

typedef struct daemon_opts
{
	int min;
	int period;
	time_t last_start;
} *daemon_t;

#define c(sv) ((daemon_t)((sv)->classinfo))

static int daemon_opt(service_t sv, char *opt)
{
	/* understand min= period= */
	if (strncmp(opt, "min=", 4)==0)	{
		c(sv)->min = atoi(opt+4);
		return 1;
	}
	if (strncmp(opt, "period=", 7) == 0) {
		char *cp = opt+7;
		int num = atoi(cp);
		if (num==0) num=1;
		while (isdigit(*cp)) cp++;
		switch(*cp) {
		case 0: break;
		case 's': break;
		case 'm': num *= 60; break;
		case 'h': num *= 3600 ; break;
		case 'd': num *= 24*3600 ; break;
		default: error("bad period specifier, %s", opt); break;
		}
		c(sv)->period = num;
		return 1;
	}
	return 0;
}


static void daemon_register(service_t sv)
{
	/* nothing to do.. */
	c(sv)->last_start = 0;
}

static void daemon_unregister(service_t sv)
{
	/* nothing to do... */
}

static int daemon_prefork(service_t sv)
{
	return 0;
}

static void daemon_check(service_t sv)
{
	/* make sure min are running, and watch for next period */
	char *env[3];
	env[0] = "METAD_REASON=min";
	env[1] = "METAD_ARG=";
	env[2] = NULL;
	while (c(sv)->min > 0 && count_procs(sv) < c(sv)->min)
		if (new_proc(sv, env)<=0)
			break;

	if (c(sv)->period > 0 &&
	    c(sv)->last_start + c(sv)->period <= time(0)) {
		env[0] = "METAD_REASON=period";
		new_proc(sv, env);
		c(sv)->last_start = time(0); /* even if it didn't start, we tried */
	}
	if (c(sv)->period > 0)
		waituntil(c(sv)->last_start + c(sv)->period);
}

static void daemon_init(service_t to)
{
	/* create ->classinfo with defaults */
	daemon_t n;

	n = (daemon_t)malloc(sizeof(struct daemon_opts));
	n->min = 0;
	n->period = 0;
	n->last_start = 0;
	to->classinfo = n;
}

static void daemon_copy(service_t from, service_t to)
{
	/* copy the 'state' classinfo - last_start */

	c(to)->last_start = c(from)->last_start;
}

static void daemon_freestate(service_t sv)
{
	free(sv->classinfo);
}


static void daemon_newparent(service_t sv, proc_t p)
{
	c(sv)->last_start = time(0);
}

static void daemon_newchild(service_t sv)
{
}

static void daemon_send(service_t sv)
{
	/* send min, period, last_start */
	send_byte(1); /* daemon */
	send_int(c(sv)->min);
	send_int(c(sv)->period);
	send_int(c(sv)->last_start);
}

struct class daemon_class = {
	.class		= "daemon",
	.c_process_opt	= daemon_opt,
	.register_service = daemon_register,
	.c_check_service= daemon_check,
	.init_state	= daemon_init,
	.copy_state	= daemon_copy,
	.free_state	= daemon_freestate,
	.send_class	= daemon_send,
	.disable_service= daemon_unregister,
	.new_parent	= daemon_newparent,
	.new_child	= daemon_newchild,
	.prefork	= daemon_prefork,
};
