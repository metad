
#define IN_ERROR
#include 	"metad.h"
#include	<stdio.h>
#include	<stdarg.h>
#define STDARGS
/* following for irix6 !! */
#define _VA_FP_SAVE_AREA 0
static int error_dest = ERROR_STDERR;
static char **error_str = NULL;
int err_str_len;

int errors_to(int where, char **place)
{
	int rv = error_dest;
	error_dest = where;
	if (where == ERROR_STRING)
		error_str = place;
	if (rv == ERROR_SYSLOG && where != ERROR_SYSLOG)
		closelog();
	if (where == ERROR_SYSLOG && rv != ERROR_SYSLOG)
	{
#ifdef LOG_DAEMON
		openlog("metad", LOG_PID, LOG_DAEMON);
#else
		openlog("metad", 0);
#endif
	}
	return rv;
}

void error(char *mesg, char *a, char *b, char *c)
{
	char buf[1024];

	sprintf(buf, mesg, a, b, c);

	switch(error_dest)
	{
	case ERROR_STDERR:
		fprintf(stderr, "metad: %s\n", buf);
		break;
	case ERROR_STRING:
		if (*error_str == NULL)
		{
			*error_str = (char*)malloc(err_str_len=(strlen(buf)+100));
			strcpy(*error_str, buf);
		}
		else if (strlen(*error_str)+strlen(buf)+1 > err_str_len)
		{
			*error_str = (char*)realloc(*error_str, err_str_len += strlen(buf)+100);
			strcat(*error_str, buf);
		}
		else
			strcat(*error_str, buf);
		break;
	case ERROR_SYSLOG:
		syslog(LOG_ERR, "%s", buf);
		break;
	}
}

#ifdef STDARGS
void logmsg(int level,...)
#else
void logmsg(va_alist)
va_dcl
#endif
{
	va_list pvar;
	char buf[1024];
	char *format;

#ifdef STDARGS
	va_start(pvar, level);
#else
	int level;
	va_start(pvar);
	level = va_arg(pvar, int);
#endif
	format = va_arg(pvar, char *);
	vsprintf(buf, format, pvar);
	switch(error_dest)
	{
	case ERROR_STDERR:
		fprintf(stderr, "metad: %s\n", buf);
		break;
	case ERROR_STRING:
		if (*error_str == NULL)
		{
			*error_str = (char*)malloc(err_str_len=(strlen(buf)+100));
			strcpy(*error_str, buf);
		}
		else if (strlen(*error_str)+strlen(buf)+1 > err_str_len)
		{
			*error_str = (char*)realloc(*error_str, err_str_len += strlen(buf)+100);
			strcat(*error_str, buf);
		}
		else
			strcat(*error_str, buf);
		break;
	case ERROR_SYSLOG:
		syslog(level, "%s", buf);
		break;
	}
}

void dolog(service_t sv, proc_t p, char *buf)
{
	logmsg(LOG_INFO, "%s: %d: %s\n", sv->service, p->pid, buf);
}

