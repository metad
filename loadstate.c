
/*
 * load process state from a file.
 * when metad reruns itself, it saves state in the form
 * of a "list" output.
 * we now read that it and add proc entries as appropriate
 *
 */
#include	"metad.h"
#include	<fcntl.h>
#ifdef SOLARIS
#include	<sys/fcntl.h>
#else
#include	<sys/file.h>
#endif
#include	"skip.h"

void qfree(char *a)
{
	if (a) free(a);
}

void loadstate(int fd)
{
	int len = lseek(fd, 0, 2);
	int b;
	char *buf;
	lseek(fd, 0, 0);

	buf = (char*)malloc(len);
	read(fd, buf, len);
	close(fd);

	init_recv(buf);
	if (get_byte() != 3) return; /* something VERY wrong */
	b = get_byte();
	while ( b==1 || b == 2) { /* service */

		char *sname;
		int enabled;
		int args;
		int class;
		service_t sv, *svp = NULL;

		sname = get_str();
		if (sname) {
			svp = skip_search(services, sname);
			free(sname);
		}
		get_int();	/* max */
		qfree(get_str());	/* home */
		qfree(get_str());	/* user */
		qfree(get_str());	/* crash */
		if (b == 2) {
			get_int(); /* watch_output */
			qfree(get_str());	/* pidfile */
		}
		get_int();	/* cnt */
		enabled = get_int();
		qfree(get_str());	/* prog */
		args = get_int();
		while (args--)
			qfree(get_str());
		class = get_byte();
		switch(class) {
		case 1:
			get_int();
			get_int();
			get_int();
			break;
		case 2:
			get_int();
			get_int();
			get_int();
			get_int();
			break;
		}
		if (svp) sv=  *svp ;else sv= NULL;
		if (sv) sv->enabled = enabled;
		b = get_byte();
		while (b == 3 || b == 4) { /* process */

			int pid, start, xit;
			int forkedpid = 0, pipefd = -1;
			pid = get_int();
			if (b == 4) {
				forkedpid = get_int();
				pipefd = get_int();
			}

			start = get_int(); /* start */
			get_int(); /* hold */
			xit = get_int();
			get_int(); /* status */
			if ((sv && (xit == 0 && kill(pid, 0)==0)) ||
			    (xit>0 && forkedpid>0 && kill(forkedpid,0)==0) ) {
				proc_t p = (proc_t)malloc(sizeof(struct proc));
				p->pid = pid;
				p->service = sv;
				p->pipefd = pipefd;
				p->bufptr = 0;
				p->it_forked = forkedpid;
				p->is_crash = 0;
				p->start_time = start;
				p->hold_time = xit?1:0;
				p->exit_time = xit;
				skip_insert(sv->proc_list, p);
				skip_insert(allprocs, p);
			}
			b = get_byte();
		}
	}
	free(buf);
}

extern char **gargv;
void restart(void)
{
	int fd;
	int len;
	char *buf;
	service_t *svp;
	char *file = "/var/tmp/...metad-temp-file";

	fd = open(file, O_RDWR|O_TRUNC|O_CREAT, 0600);
	if (fd < 0) {
		close(0);
		execv(gargv[2], gargv);
		exit(1);
	}
	unlink(file);
	init_return();
	send_byte(3);		/* listing */
	for (svp = skip_first(services) ; svp ; svp = skip_next(svp)) {
		send_service(*svp);
	}
	send_byte(0);		/* finished */

	buf = get_return(&len);
	write(fd, buf, len);
	if (fd > 0) {
		close(0);
		dup(fd);
		close(fd);
	}
	gargv[0] = "metad-restart";
	execv(gargv[2], gargv);
	exit(1);
}
