

#include	"metad.h"
#include	<signal.h>
#include	"skip.h"
#include	<sys/time.h>

/*
 * main loop of metad
 * wait for read-select on some sockets, or for time to pass
 * or for a process to exit.
 * when one of these happens, we check with each service to see
 * if it wants to do something.
 *
 * services ask to do something with
 *  listenon(socket)
 *  waituntil(time)
 * and can check is a socket is read with
 *  readyon(socket)
 *
 */


static fd_set wait_for, are_ready;
static fd_set write_on, can_write;
static time_t when_wake;

void listenon(int socket)
{
	FD_SET(socket, &wait_for);
}

int readyon(int socket)
{
	return FD_ISSET(socket, &are_ready);
}

void writeon(int socket)
{
	FD_SET(socket, &write_on);
}



int canwrite(int socket)
{
	return FD_ISSET(socket, &can_write);
}

void waituntil(time_t when)
{
	if (when_wake == 0 || when_wake > when) when_wake = when;
}

static struct
{
	pid_t pid;
	int status;
	time_t time;
} saved_pids[20] = { { 0 } };

static struct timeval select_tv;
static void collectchild()
{
	pid_t pid;
	int status;

	if ((pid = waitpid(-1, &status, WNOHANG)) > 0)
	{
		int i;
		for (i=0; i<20 ; i++)
			if (saved_pids[i].pid == 0)
			{
				saved_pids[i].pid = pid;
				saved_pids[i].status = status;
				time(&saved_pids[i].time);
				break;
			}
	}
	select_tv.tv_usec = 0;
	select_tv.tv_sec = 0;
}

int is_saved_pid(pid_t pid)
{
	int i;
	for (i=0; i<20; i++)
		if (saved_pids[i].pid == pid)
			return 1;
	return 0;
}

void main_loop()
{
	struct sigaction sa;
	sigset_t ss;
	sa.sa_handler = collectchild;
	sa.sa_flags = 0;
	sigemptyset(&ss);
	sigaddset(&ss, SIGCLD);
	sa.sa_mask = ss;
	sigaction(SIGCLD, &sa, NULL);

	FD_ZERO(&are_ready);
	FD_ZERO(&can_write);
	while (1)
	{
		service_t *svp;
		int i;
		FD_ZERO(&wait_for);
		FD_ZERO(&write_on);
		when_wake = time(0)+5*60; /* always wakeup every 5minutes to look around... */
		collectchild(); /* incase signal was missed */

		for (i=0 ; i<20 ; i++)
			if (saved_pids[i].pid)
			{
				proc_t *pp;
				pp = skip_search(allprocs, &saved_pids[i].pid);
				if (pp)
				{
					(*pp)->status = saved_pids[i].status;
					(*pp)->exit_time = saved_pids[i].time;;
					select_tv.tv_sec = 0;
					logmsg(LOG_INFO, "process %d (%s) exited - status 0x%04x", saved_pids[i].pid, (*pp)->service->service, (*pp)->status);
				} else
					logmsg(LOG_INFO, "process %d exited - status 0x%04x", saved_pids[i].pid, saved_pids[i].status);
				saved_pids[i].pid = 0;

			}

		for (svp = skip_first(services) ; svp ; svp = skip_next(svp))
			check_service(*svp);
		check_control();
		are_ready = wait_for;
		can_write = write_on;

		if (when_wake)
		{
			time_t now;
			time(&now);
			select_tv.tv_sec = when_wake - now;
			select_tv.tv_usec = 0;
			if (when_wake < now) select_tv.tv_sec = 0;
		}
		else
		{
			select_tv.tv_sec = 100000;
			select_tv.tv_usec = 0;
		}
		if (select(FD_SETSIZE, &are_ready, &can_write, NULL, &select_tv) <= 0)
		{
			FD_ZERO(&are_ready);
			FD_ZERO(&can_write);
		}
	}
}

