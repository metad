
/* send commands to metad's
 * -V   display meta version and metad versions
 *
 * -u	send datagrams, don't use stream connection
 * -b   broadcast datagrams on all interfaces
 *
 * -r service run the service
 *  -a args   pass args with previous run command
 * -k service kick(run) the service
 * -l service list the service
 * -L         list all services
 * -D service disable service
 * -E service enable service
 * -K pid/service kill process/service (Sig 15)
 * -R	      reread config
 * -X         tell metad to exit
 * -v         be verbose
 * -C command send the given command
 *
 * -B	      actually send broadcast requests
 *
 * other arguments are host names
 *
 */

#include	<stdio.h>
#include	<string.h>
#include	"metad.h"
#include	"args.h"
#include	"dlink.h"

char *progname;

void usage(char *fmt, char *arg)
{
	char buf[1024];
	sprintf(buf, fmt, arg);
	fprintf(stderr,"%s: %s\n", progname, buf);
	fprintf(stderr,"Usage: %s -[h?Vubv] -[LRXB] {-[rklDEK] service} [-C command] hostlist ...\n", progname);
}

void help()
{
	printf(
		"Usage: %s -[h?Vubv] -[LRXB] {-[rklDEK] service} [-C command] hostlist ...\n"
		"       -h          This help message\n"
		"       -?          As above\n"
		"       -V          Print version number of metac and the metad running on any\n"
		"                   host listed\n"
		"       -u          Send commands in udp datagrams. This is faster than the\n"
		"                   normal tcp, but\n"
		"                   avoids delays on dead machines.\n"
		"       -b          Send each request as a broadcast on any available network\n"
		"                   interfaces.\n"
		"       -v          Be verbose when listing services.\n"
		"       -L          List all services supported by the metad on the remote host.\n"
		"       -R          Tell metad to reread it's configuration file.\n"
		"       -X          Tell metad to eXit.\n"
		"       -B          Tell the metad to rebroadcast the request on it's\n"
		"                   interfaces.\n"
		"       -r service  Try to start a process running for the service.\n"
		"       -k service  Kick a service into action, same as above.\n"
		"       -l service  List details of the named service.\n"
		"       -D service  Disable a service. No more processes will be started for\n"
		"                   that service.\n"
		"       -E service  Enable a service. Metad will start processes as\n"
		"                   appropriate.\n"
		"       -K pid      Of metad is managing a process with number pid, then it\n"
		"                   is sent a SIGTERM.\n"
		"       -K service  Every process running for the given service is sent a\n"
		"                   SIGTERM.\n"
		"       -C command  Send the given command (which will usually be quoted).\n"
		"    Commands are:\n"
		"       run service args       like -r, but allows args to be passed to\n"
		"                              the process.\n"
		"       kill signum pid-or-service  like -K, but allows a signal number\n"
		"                             to be given.\n"
		"       restart               tell metad to re-exec itself, for use when a\n"
		"                             new version is installed\n"
		"     (plus others that duplicate the above flags)\n"
		, progname)  ;
}

int main(int argc, char *argv[])
{
	void *cmds = dl_head();
	void *hosts = dl_head();
	int local_broad = 0;
	int remote_broad = 0;
	int use_dgram = 0;
	int show_version = 0;
	int show_help = 0;
	int verbose = 0;
	char *c, *c2;
	void *opts = args_init(argc, argv, "??h?V?u?b?r:a:k:l:L?D:E:K:R?X?v?C:B?");
	int opt;
	char *arg;
	int pos, inv;
	progname = strrchr(argv[0], '/');
	if (progname) progname++; else progname = argv[0];

	while ((opt = args_next(opts, &pos, &inv, &arg))!= END_ARGS)
		switch(opt)
		{
		case BAD_FLAG:
			usage("Bad option: %s", argv[pos]);
			exit(1);
		case NO_FLAG:
			c = dl_strdup(arg);
			dl_add(hosts, c);
			break;
		case 'h':
		case '?':
			show_help = 1;
			break;
		case 'V':		/* my version number */
			printf("%s: version %s\n", progname, version);
			show_version  =1;
			break;
		case 'u':
			use_dgram = 1;
			break;
		case 'b':
			local_broad = 1;
			break;
		case 'r':
		case 'k':
			c = dl_strndup("kick ", 5+strlen(arg)+1);
			strcat(c, arg);
			dl_add(cmds, c);
			break;
		case 'a':
			c = dl_prev(cmds);
			if (c == cmds || (strncmp(c, "kick ", 5)!= 0 && strncmp(c, "run ", 4)!= 0))
			{
				usage("%s: -a only valid after -r or -k", NULL);
				exit(1);
			}
			c2 = dl_strndup(c, strlen(c)+1+strlen(arg)+1);
			strcat(c2, " ");
			strcat(c2, arg);
			dl_del(c); dl_free(c);
			dl_add(cmds, c2);
			break;
		case 'l':
			c = dl_strndup("list ", 5+strlen(arg)+1);
			strcat(c, arg);
			dl_add(cmds, c);
			break;
		case 'L':
			c = dl_strdup("list");
			dl_add(cmds, c);
			break;
		case 'D':
			c = dl_strndup("disable ", 8+strlen(arg)+1);
			strcat(c, arg);
			dl_add(cmds, c);
			break;
		case 'E':
			c = dl_strndup("enable ", 7+strlen(arg)+1);
			strcat(c, arg);
			dl_add(cmds, c);
			break;
		case 'K':
			c = dl_strndup("kill 15 ", 8+strlen(arg)+1);
			strcat(c, arg);
			dl_add(cmds, c);
			break;
		case 'R':
			c = dl_strdup("reread");
			dl_add(cmds, c);
			break;
		case 'X':
			c = dl_strdup("die");
			dl_add(cmds, c);
			break;
		case 'v':
			verbose = 1;
			break;
		case 'C':
			c  = dl_strdup(arg);
			dl_add(cmds, c);
			break;
		case 'B':
			remote_broad = 1;
			break;
		}

	if (show_help) {
		help();
		exit(0);
	}
	if (dl_next(hosts) == hosts && local_broad == 0) {
		/* no where to send to... */
		if (dl_next(cmds) != cmds) {
			fprintf(stderr,"%s: commands were specified with no where to send them!\n", progname);
			exit(1);
		}
		/* nothing to do, though might has displayed version.
		 * Should probably give usage...
		 */
		if (show_version == 0)
			usage("Nothing to do", NULL), exit(1);
		exit(0);
	}

	if (dl_next(hosts) != hosts && local_broad) {
		fprintf(stderr, "%s: you probably don't want to broadcast AND list hosts...\n", progname);
		exit(1);
	}

	if (show_version) {
		c = dl_strdup("version");
		dl_insert(cmds, c);
	}
	for (c= dl_next(cmds) ; c != cmds ; c = dl_next(c)) {
		char *cmd = (char*)malloc(10 + strlen(c)); /* make sure there is room for port and broad */
		char *h;

		strcpy(cmd, "1 ");
		if (remote_broad)
			strcat(cmd, "BROAD ");
		strcat(cmd, c);
		if (local_broad)
			broadcast(cmd);
		else for (h=dl_next(hosts) ; h != hosts ; h = dl_next(h))
		{
			if (strcmp(h, ".")== 0)
				send_cmd(cmd, use_dgram, "localhost", verbose);
			else
				send_cmd(cmd, use_dgram, h, verbose);
		}
	}
	exit(0); /* FIXME what exit status */
}
