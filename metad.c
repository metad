
#include	"metad.h"
#include	<sys/ioctl.h>
#include	<sys/fcntl.h>
#ifdef SOLARIS
#include	<sys/termios.h>
#endif

char **gargv; /* used to pass args to 'restart' */

int main(int argc, char *argv[])
{
	gargv = argv;

	{
		int ttyfd = open("/dev/tty", 2, 0);
		if (ttyfd >= 0) {
			ioctl(ttyfd, TIOCNOTTY, NULL);
			close(ttyfd);
		}
	}

	/* FIXME should make sure stdin/stdout/stderr are open to something */
	service_init();
	control_init();
	errors_to(ERROR_SYSLOG, NULL);
	read_config(services, argv[1]);
	if (strcmp(argv[0], "metad-restart")==0) {
		loadstate(0);
		open("/dev/null", O_RDONLY);
	}
	main_loop();
	exit(0);
}
