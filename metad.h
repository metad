
#include	<sys/types.h>
#include	<unistd.h>
#include	<stdlib.h>
#include	<sys/wait.h>
#include	<string.h>
#include	<syslog.h>
#include	<ctype.h>
#include	<signal.h>
#include	<time.h>
/* hold config file info */


typedef struct service {
	char	*service;		/* name of service */
	struct class *class;		/* pointer to class*/
	void	*classinfo;		/* class specific options */
	/* class independant options */

	int	max_proc;		/* max number of processes */
	char	*crash_prog;		/* prog to call when process crashes */
	char	*home_dir;		/* directory to run process in */
	char	*username;		/* who to run process as */
	char	*pidfile;		/* file to read process id from after child exits */
	int	watch_output;		/* if true, attatch a pipe to std{out,err} and what it */
	int	enabled;		/* whether to start enabled */
	int	start_cnt;
	char	*program;		/* program to run */
	char	**args;			/* arguments */

	int	pending;		/* set before reprocessing config file, cleared when found in file */

	void *proc_list;		/* skiplist of currently active processes */
	time_t next_hold;		/* hold time for next crashing processes */
} *service_t;

typedef struct proc {
	pid_t		pid;		/* pid of process */
	service_t	service;
	int		pipefd;		/* if a pipe was conencted to stdout/stderr */
	char		pipebuf[300];	/* buffer lines of data from pipefd before syslogging */
	int		bufptr;		/* how full buf is */
	int		it_forked;	/* true if process fork/exited, and we have pid from pidfile */
	int 		is_crash;	/* if cleaning up after core dump */
	time_t		start_time;	/* when processes started */
	time_t		hold_time;	/* time to let go of processes slot - set if process crashes early */
	time_t		exit_time;	/* the time that it exited */
	int		status;		/* wait status if exit_time > 0 */
} *proc_t;

typedef struct class {
	char *class;			/* name of class */
	int  (*c_process_opt)(service_t sv, char *opt);	/* function to processes options */
	void (*register_service)(service_t sv);	/* register the service if necessary */
	void (*c_check_service)(service_t sv);	/* check if anything needs to be done for a service */
	void (*init_state)(service_t to);	/* copy state from old service struct to new */
	void (*copy_state)(service_t to, service_t from);/* copy state from old service struct to new */
	void (*free_state)(service_t sv);	/* free class dependant state */
	void (*send_class)(service_t sv);	/* send class info */
	void (*disable_service)(service_t sv);	/* unregister service */
	void (*new_parent)(service_t sv, proc_t p);/* in parent of new child */
	void (*new_child)(service_t sv);	/* in a new child */
	int  (*prefork)(service_t sv);		/* just before fork */
} *class_t;


typedef struct daemon_info { 		/* class specific info for class "daemon" */
	int		min_proc;	/* minimum number of processes to have running */
	int		period;		/* period in seconds for restarts	*/

	time_t	last_restart;		/* last time a periodic restart was attempted */
} *daemon_info_t;


typedef struct commands {
	char *	name;
	void	(*proc)();
} *commands_t;

class_t find_class(char *name);
void loadstate(int fd);
int do_command(char **args, char *host, void *con);
int tcp_port();
int udp_port();
int control_init(void);
void check_control(void);
#ifndef IN_CONTROL
int set_reply(void *con, char *reply, int len);
int return_error(void *con, char *fmt, ...);
#endif
void listenon(int socket);
int readyon(int socket);
void writeon(int socket);
int canwrite(int socket);
void waituntil(time_t when);
void main_loop(void);
int read_config(void *services, char *file);
int count_procs(service_t sv);
void check_service(service_t sv);
int new_proc(service_t sv, char **env);
service_t find_service(char *name);
service_t new_service(char *name, class_t class);
int process_opt(service_t sv, char *opt);
void service_init(void);
void free_service(service_t sv);
void broadcast(char *line);
void init_return(void);
void send_byte(int b);
int get_byte(void);
void send_int(int i);
int get_int(void);
void send_str(char *);
void send_service(service_t);
void do_send(void *);
void control_close(void);
void prepare_restart(void);
void restart(void);
void init_recv(char*);
char *prtime(time_t);
int udp_port(void);
int tcp_port(void);
void nodelay(int socket);

int send_cmd(char *cmd, int udp, char *host, int verbose);

char *get_str();
char *get_return();

#ifndef IN_ERROR
void error(char *mesg,...);
void logmsg(int, char*, ...);
#endif
#define	ERROR_STDERR	0
#define	ERROR_SYSLOG	1
#define	ERROR_STRING	2
int errors_to(int where, char **place);

char **strlistdup(char **l);
void strlistfree(char **l);
char **strsplit(char *line, char *sep);

void dolog(service_t, proc_t, char *);

extern void *services;
extern void *allprocs;

extern int is_saved_pid(pid_t pid);
extern char version[];
