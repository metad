
#include	<sys/types.h>
#include	<unistd.h>
#include	<netdb.h>
#include	<netinet/in.h>	/* for hton? */

int tcp_port()
{
	struct servent *sv;
	if (geteuid() > 0)
		return htons(6110);
	sv = getservbyname("metad","tcp");
	if (sv)
		return sv->s_port;
	else
		return htons(611);
}

int udp_port()
{
	struct servent *sv;
	if (geteuid() > 0)
		return htons(6110);
	sv = getservbyname("metad","udp");
	if (sv)
		return sv->s_port;
	else
		return htons(611);
}
