
#include	<sys/param.h>
#include	<unistd.h>
#include	<stdio.h>


#define	WEEKS	(DAYS * 7L)
#define	DAYS	(HOURS * 24L)
#define	HOURS	(MINUTES * 60L)
#define	MINUTES	60L

/* static function declarations */


char *prtime(time_t time)
{
	static	char tim[9];
	if (time >= WEEKS) {
		sprintf(tim,"%4ldw%02ldd", time / WEEKS, (time % WEEKS) / DAYS);
	} else if (time >= DAYS) {
		sprintf(tim,"%4ldd%02ldh", time / DAYS , (time % DAYS) / HOURS);
	} else if (time >= HOURS) {
		sprintf(tim,"%4ldh%02ldm", time / HOURS , (time % HOURS) / MINUTES);
	} else if (time >= MINUTES) {
		sprintf(tim,"%4ldm%02lds", time / MINUTES , time % MINUTES);
	} else
		sprintf(tim,"%7ds", (int)time);
	return tim;
}
