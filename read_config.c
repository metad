
#include	"metad.h"
#undef NULL
#include	<stdio.h>
#include	"skip.h"

/* read the config file
 *
 * first mark all services as pending
 *
 * then for each line which is not a blank/comment
 *  strplit and find name/class
 *  if name already exists:
 *    if same class, set attributes
 *    if different class, rename old service and create new
 *  else
 *    create new service if remainder parses ok
 *
 * if any remaining services are pending, disabled them unless there were errors
 *
 */

int read_config(void *services, char *file)
{
	service_t *serv;
	char linebuf[1024];
	FILE *f;
	int err = 0;
	static char *lastfile = NULL;
	if (file) lastfile = file;
	else file = lastfile;

	for (serv = skip_first(services) ; serv ; serv = skip_next(serv))
		(*serv)->pending = 1;

	f = fopen(file, "r");
	if (f == NULL) {
		error("cannot find config file %s", file);
		return 1;
	}
	while (fgets(linebuf, sizeof(linebuf), f)!= NULL) {
		int len = strlen(linebuf);
		char **words;
		int w;

		if (len > 1000 && linebuf[len-1] != '\n') {
			error("line too long in config file");
			fclose(f);
			return 1;
		}
		if (linebuf[len-1] == '\n') linebuf[--len] = 0;

		words = strsplit(linebuf, " \t\n");
		if (words) {
			for (w=0 ;words[w] ; w++)
				if (words[w][0]=='#')
					words[w]=NULL;
			if (words[0] && words[1]) {
				/* not a comment */
				service_t *svp = skip_search(services, words[0]);
				service_t sv;
				class_t cl = find_class(words[1]);
				if (svp != NULL &&
				    (*svp)->class != cl) {
					/* different  classes - rename old services */
					skip_delete(services, words[0]);
					strcpy(linebuf, words[0]);
					do { strcat(linebuf, ".old"); } while (skip_search(services, linebuf)!= NULL);
					free((*svp)->service);
					(*svp)->service = strdup(linebuf);
					skip_insert(services, *svp);
					svp = NULL;
				}
				if (cl == NULL) {
					error("unknown class %s", words[1]);
					err ++;
					continue;
				}
				sv = new_service(words[0], cl);
				/* now process options */
				for (w=2 ; words[w] && words[w][0] != '/' ; w++) {
					int ok = process_opt(sv, words[w]);
					if (ok == 0)
						ok = sv->class->c_process_opt(sv, words[w]);
					if (ok <= 0) {
						if (ok == 0)
							error("unknown option: %s", words[w]);
						else
							error("error in option: %s", words[w]);
						free_service(sv);
						sv = NULL;
						err++;
						break;
					}
				}
				if (sv == NULL)
					continue;
				if (words[w])
					sv->program = strdup(words[w++]);
				if (words[w])
					sv->args = strlistdup(words+w);
				if (sv->program == NULL)
					error("missing program name for service %s", words[0]), err++;
				else if (sv->args == NULL)
					error("missing program arguments for service %s", words[0]), err++;
				else {
					if (svp) {
						proc_t *pp2;
						service_t sv2 = *svp;
						sv->enabled = sv2->enabled;
						sv->proc_list = sv2->proc_list;

						/* change service pointer to the new structure (for aidan) */
						if (sv->proc_list)
							for (pp2 = skip_first(sv->proc_list) ; pp2 ; pp2=skip_next(pp2))
								(*pp2)->service = sv;

						sv2->proc_list = NULL;
						sv->class->copy_state(sv2, sv);
						skip_delete(services, sv2->service);
						free_service(sv2);
					}
					skip_insert(services, sv);
					sv->class->register_service(sv);
					sv = NULL;
				}
				if (sv) free_service(sv);
			}
			free(words[-1]);
			free(words-1);
		}
	}
	fclose(f);
	for (serv = skip_first(services) ; serv ; serv = skip_next(serv))
		if ((*serv)->pending)
			(*serv)->enabled = 0;
	return err;
}
