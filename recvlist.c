#include <malloc.h>

char *recvbuf;
int recvptr;

void init_recv(char *buf)
{
	recvbuf  = buf;
	recvptr = 0;
}

int get_byte()
{
	return 0xff & recvbuf[recvptr++];
}

int get_int()
{
	int i = 0;
	i = get_byte();
	i = (i<<8) | get_byte();
	i = (i<<8) | get_byte();
	i = (i<<8) | get_byte();
	return i;
}

char *get_str()
{
	int l = get_int();
	char *s,*p;
	if (l == 0)
		return (char*)0;
	p = s = (char*)malloc(l);
	while (l--)
		*p++ = get_byte();
	return s;
}
