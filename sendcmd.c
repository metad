
/* send a command to a metad somewhere
 * if we use tcp, wait for reply and interpret it
 */

#include	<sys/types.h>
#include	<sys/socket.h>
#include	<netinet/in.h>
#include	<netinet/tcp.h>
#include	<arpa/inet.h>
#include	<netdb.h>
#include	<stdio.h>
#include	<string.h>
#include	<time.h>
#include	"skip.h"

#include "metad.h"

char *get_str(void);
/* cache addresses */
typedef struct acache
{
	char *host;
	struct sockaddr_in addr;
} *acache;

extern char *progname;

static int acmp(acache a, acache b, char *host)
{
	if (b) host=b->host;
	return strcmp(a->host, host);
}
static void afree(acache a)
{
	free(a->host);
	free(a);
}


static void *addr_cache = NULL;

static void list_service(char *, int);

int send_cmd(char *cmd, int udp, char *host, int verbose)
{

	acache *cp, c;
	if (addr_cache == NULL)
		addr_cache = skip_new(acmp, afree, NULL);

	cp = skip_search(addr_cache, host);
	if (cp != NULL)
		c = *cp;
	else
	{
		c = (acache)malloc(sizeof(struct acache));
		c->host = strdup(host);
		c->addr.sin_port = udp ? udp_port() : tcp_port();
		c->addr.sin_addr.s_addr = inet_addr(host);
		if (c->addr.sin_addr.s_addr > 0 && c->addr.sin_addr.s_addr < 0xffffffff)
		{
			c->addr.sin_family = AF_INET;
		}
		else
		{
			struct hostent *he;
			he = gethostbyname(host);
			if (he == NULL)
			{
				c->addr.sin_family = -1;
				fprintf(stderr, "%s: unknown host %s\n", progname, host);
			}
			else
			{
				memcpy(&c->addr.sin_addr, he->h_addr_list[0], 4);
				c->addr.sin_family = AF_INET;
			}
		}
		skip_insert(addr_cache, c);
	}
	if (c->addr.sin_family != AF_INET)
		return -1;
	if (udp)
	{
		static int sock = -1;

		if (sock == -1)
		{
			sock = socket(AF_INET, SOCK_DGRAM, 0);
			if (geteuid() == 0)
			{
				int port;
				struct sockaddr_in myaddr;
				port = 600 + time(0)%400;

				memset(&myaddr, 0, sizeof(myaddr));
				myaddr.sin_family = AF_INET;
				while (port > 500)
				{
					myaddr.sin_port = htons(port);
					if (bind(sock, (struct sockaddr *)&myaddr, sizeof(myaddr))== 0)
						break;
					port --;
				}
				if (port == 500)
				{
					fprintf(stderr, "%s: cannot bind priv udp port...\n", progname);
				}
			}
		}
		sendto(sock, cmd, strlen(cmd)+1, 0, (struct sockaddr *)&c->addr, sizeof(c->addr));
	}
	else
	{
		/* tcp - have to make a new connection each time */
		int sock;
		static int port = 0;
		char buf[8192]; /* FIXME autosize */
		int n;
		int have = 0;

		if (port == 0)
			port = 600 + time(0)%400;

		if (geteuid() == 0)
			sock = rresvport(&port);
		else
			sock = socket(AF_INET, SOCK_STREAM, 0);
		if (sock == -1)
		{
			fprintf(stderr, "%s: cannot bind socket!!\n", progname);
			return -1;
		}
#ifdef TCP_CONN_ABORT_THRESHOLD
		{
			int thresh;
			thresh = 10*1000;
			setsockopt(sock, IPPROTO_TCP, TCP_CONN_ABORT_THRESHOLD, (char*)&thresh, 4);
		}
#endif
/* */
		if (connect(sock, (struct sockaddr *)&c->addr, sizeof(c->addr))!= 0)
		{
			fprintf(stderr, "%s: cannot connect to %s\n", progname, c->host);
			close(sock);
			c->addr.sin_family = -1;
			return -1;
		}
		write(sock, cmd, strlen(cmd)+1);
		shutdown(sock, 1); /* don't want to write no more */
		do
		{
			n = read(sock, buf+have, sizeof(buf)-1 - have);
			if (n>0) have += n;
		} while (n>0 && have < sizeof(buf)-1);
		close(sock);
		if (have <= 0)
			return 0; /* probably OK, FIXME */
		buf[have] = 0;
		switch(buf[0])
		{
		case 0: return 0; /* definately ok */
		case 1: /* error message */
			fprintf(stderr, "%s: %s: %s\n", progname, c->host, buf+1);
			return 1;
		case 2: /* version number */
			fprintf(stdout, "%s: %s\n", c->host, buf+1);
			return 0;
		case 3: /* listing */
			init_recv(buf+1);
			list_service(c->host, verbose);
			/* FIXME */
			return 0;

		case 'm': /* old version number */
			fprintf(stdout, "%s\n", buf);
			return 0;
		default: /* old metad */
			/* FIXME */
			return 0;
		}
	}
	return 0;
}


static void list_service(char *host, int verbose)
{
	int b;
	b = get_byte();
	while (b == 1 || b == 2)		/* a service */
	{
		char *sname, *home, *user, *crash, *prog;
		char **args;
		int argc;
		int max, cnt, enabled;
		int i;
		int class;
		char *classname=NULL;
		char *pidfile = NULL;
		int watch_output = 0;
		int min=0, period=0, last;
		int proto, port=0, active=0, backlog=0;

		sname = get_str();
		max = get_int();
		home = get_str();
		user = get_str();
		crash = get_str();
		if (b == 2)
		{
			watch_output = get_int();
			pidfile = get_str();
		}
		cnt = get_int();
		enabled = get_int();
		prog = get_str();
		argc = get_int();
		args = (char**)malloc((argc+1)*sizeof(char*));
		for (i=0 ; i<argc ; i++)
			args[i] = get_str();
		args[i] = NULL;
		class = get_byte();
		switch(class)
		{
		case 1:			/* daemon */
			classname = "daemon";
			min = get_int();
			period = get_int();
			last = get_int();
			break;
		case 2:			/* stream */
			classname = "stream";
			proto = get_int();
			port = get_int();
			backlog = get_int();
			active = get_int();
			break;
		}
/*	printf("Host: %s\n", host); */
		if (sname == NULL) sname = "*unknown*";
		printf("%s:%s%*.0s %s %s (x%d)", host, sname, 18-(int)strlen(host) - (int)strlen(sname), "",
		       classname, enabled?"enabled":"DISABLED", cnt);
		if (verbose)
		{
			printf("\n");
			printf("   Prog: %s\n", prog);
			printf("   Args:");
			for (i=0; i<argc ; i++)
				printf(" %s", args[i]);
			printf("\n");
			printf("   Opts:");
			if (max) printf(" max=%d", max);
			if (home) printf(" dir=%s", home);
			if (user) printf(" user=%s", user);
			if (crash) printf(" crash=%s", crash);
			if (pidfile) printf(" pidfile=%s", pidfile);
			if (watch_output) printf(" watch_output");
			switch(class)
			{
			case 1:		/* daemon */
				if (min) printf(" min=%d", min);
				if (period) printf(" period=%d", period);
				break;
			case 2:
				printf(" port=%d", port);
				printf(" backlog=%d", backlog);
				if (!active) printf(" INACTIVE");
				break;

			}
			printf("\n");
		}
		b = get_byte();
		while (b == 3 || b == 4) /* process */
		{
			int pid, hold, status;
			int forkedpid;
			time_t start, xit;
			pid = get_int();
			if (b==4)
			{
				forkedpid = get_int();
				get_int(); /* pipefd */
			}
			else forkedpid = 0;
			start = get_int();
			hold = get_int();
			xit = get_int();
			status = get_int();
			if (verbose)
			{
				printf("     Proc: pid=%5d%s %s", (xit && forkedpid) ? forkedpid : pid,
				       (xit&&forkedpid)?"f":"", ctime(&start));
				if (xit && forkedpid==0)
				{
					char *t,*t2;
					time_t holdt;
					holdt = hold - time(0);
					t = prtime(holdt);
					while(*t == ' ') t++;
					t2=ctime(&xit)+4;
					printf("        Exited %1.12s holding for %s\n", t2, t);
				}
			}
			else
			{
				char *t;
				time_t age = time(0) - start;
				t = (char*)prtime(age);
				while(*t == ' ') t++;
				printf(" %d%s(%s%s)",  (xit && forkedpid) ? forkedpid : pid,
				       (xit&&forkedpid)?"f":"",
				       (xit && forkedpid==0)?"exited:":"", t);
			}
			b = get_byte();
		}
		if (!verbose) printf("\n");
	}
}
