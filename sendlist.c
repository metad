
#include	"metad.h"
#include	"skip.h"

/*
 * 3 - listing
 *  1 - service
 *  2 - service-2 with watchoutput and pidfile
 *  3 - process-1
 *  4 - process-2 with pipefd or it_forked
 */

void send_proc(proc_t p)
{
	if (p->it_forked > 0 || p->pipefd >= 0) {
		send_byte(4);
		send_int(p->pid);
		send_int(p->it_forked);
		send_int(p->pipefd);
	} else {
		send_byte(3);		/* process */
		send_int(p->pid);
	}
	send_int(p->start_time);
	send_int(p->hold_time);
	send_int(p->exit_time);
	send_int(p->status);
}

void send_service(service_t sv)
{
	int i;
	proc_t *pp;
	if (sv->watch_output || sv->pidfile)
		send_byte(2);
	else
		send_byte(1); /* service */
	send_str(sv->service);
	send_int(sv->max_proc);
	send_str(sv->home_dir);
	send_str(sv->username);
	send_str(sv->crash_prog);
	if (sv->watch_output || sv->pidfile) {
		send_int(sv->watch_output);
		send_str(sv->pidfile);
	}
	send_int(sv->start_cnt);
	send_int(sv->enabled);
	send_str(sv->program);
	for (i=0 ; sv->args[i] ; i++)
		;
	send_int(i);
	for (i=0 ; sv->args[i] ; i++)
		send_str(sv->args[i]);
	(sv->class->send_class)(sv);
	for (pp = skip_first(sv->proc_list) ; pp ; pp = skip_next(pp))
		send_proc(*pp);
}

void send_int(int i)
{
	send_byte((i>>24)& 0xff);
	send_byte((i>>16)& 0xff);
	send_byte((i>> 8)& 0xff);
	send_byte((i>> 0)& 0xff);
}

void send_str(char *s)
{
	if (s == NULL)
		send_int(0);
	else {
		send_int(strlen(s)+1);
		while (*s)
			send_byte(*s++);
		send_byte(0);
	}
}

static char *sendbuf = NULL;
static int sendsize;
static int sendptr;

void send_byte(int b)
{
	if (sendptr >= sendsize) {
		sendsize += 100;
		sendbuf = (char*)realloc(sendbuf, sendsize);
	}
	sendbuf[sendptr++] = b;
}

void init_return()
{
	sendptr = 0;
	if (sendbuf == NULL) {
		sendsize = 200;
		sendbuf = malloc(sendsize);
	}
}

void do_send(void *con)
{
	set_reply(con, sendbuf, sendptr);
	sendbuf = NULL;
	sendptr = 0;
}

char *get_return(int *i)
{
	char *c = sendbuf;
	*i = sendptr;
	sendbuf = NULL;
	sendptr = 0;
	return c;
}
