
#include	"metad.h"
#include	"skip.h"
#include	<pwd.h>
#include	<grp.h>
#include	<sys/stat.h>
#include	<fcntl.h>
#include	<errno.h>
#include	<values.h>

void *services;
void *allprocs;

int count_procs(service_t sv)
{
	proc_t *pp, p;
	int cnt;
	time_t now = time(0);
	for (cnt=0, pp=skip_first(sv->proc_list) ; pp ; pp=skip_next(pp))
	{
		p = *pp;
		if (p->exit_time == 0 || p->hold_time > now || p->pipefd >=0 || p->it_forked)
			cnt++;
	}
	return cnt;
}


void check_service(service_t sv)
{
	/* check for exited processes and possibly run crash */
	proc_t *pp;
	time_t now = time(0);

	for (pp=skip_first(sv->proc_list) ; pp ; ) {
		proc_t p;
		p = *pp;
		pp = skip_next(pp);
		if (p->exit_time == 0) {
			/* monitoring a normal child
			 * we should have been alerted if the child exitted,
			 * but just-in-case... try sig0
			 */
			if (kill(p->pid, 0) == -1 && errno == ESRCH && !is_saved_pid(p->pid)) {
				p->status = 0; /* who knows. */
				p->exit_time = now;
			}
		}
		if (p->exit_time > 0 && p->hold_time == 0) {
			/* a newly exited process */
			if (p->is_crash) {
				/* this is a crash recover prog exitting. just set the hold time */
				p->hold_time = p->exit_time + sv->next_hold;
			}
			else if (WIFSIGNALED(p->status) ||
				 (WIFEXITED(p->status) &&
				  WEXITSTATUS(p->status) != 0)) {
				/* exited badly */
				if (p->exit_time - p->start_time < 30) {
					if (sv->next_hold < MAXINT/2)
						sv->next_hold *= 2;
				} else
					sv->next_hold = 2;
				if (0 && sv->crash_prog) {
					/* FIXME */
				}
				else p->hold_time = p->exit_time + sv->next_hold;
			}
			else
			{
				if (sv->pidfile) {
					/* if this file is newer than start_time and contains a pid,
					 * record that pid as this pid and shedule regular checks
					 */
					int fd = open(sv->pidfile, 0);
					struct stat stb;
					char pidline[100];
					int n;
					int pid;
					if (fd >= 0
					    && fstat(fd, &stb) != -1
					    && stb.st_mtime >= p->start_time
					    && (n=read(fd, pidline, sizeof(pidline)-1))> 0
					    && (pidline[n]=0 , pid=atoi(pidline)) > 1
					    && kill(pid, 0) != -1
						) {
						/* looks good ! */
						p->it_forked = pid;
						p->hold_time = 1;
						waituntil(now+5); /* check it in 5 seconds and periodically */
					} else {
						/* give the program a few seconds to write to the file
						 * e.g. xdm forks, then creates the pid file
						 */
						if (p->exit_time + 10 > now) {
							p->it_forked = -1;
							waituntil(p->exit_time + 11);
						} else {
							// give up waiting
							p->it_forked = 0;

							/* probably exited badly if it didn't run for long
							   (keep in mind that the pidfile wasn't updated) */
							if (p->exit_time - p->start_time < 30) {
								if (sv->next_hold < MAXINT/2)
									sv->next_hold *= 2;
							} else
								sv->next_hold = 2;

							p->hold_time = p->exit_time + sv->next_hold;
						}
					}
					if (fd >= 0) close(fd);
				} else
					sv->next_hold = 2;

			}
		}
		else if (p->exit_time > 0 && p->it_forked > 0) {
			/* monitoring forked child
			 * try sending sig0 to see if process still exists
			 */
			if (kill(p->it_forked, 0)== -1 && errno == ESRCH) {
				p->it_forked = 0;
				p->exit_time = now;
			} else
				waituntil(now+30);	/* wait 30 seconds and check again */
		}
		if (p->pipefd >= 0) {
			if (readyon(p->pipefd)) {
				int n = read(p->pipefd, p->pipebuf+p->bufptr, sizeof(p->pipebuf)-p->bufptr-1);
				if (n<= 0) {
					close(p->pipefd);
					p->pipefd = -1;
				} else {
					int i = 0;
					p->bufptr += n;
					while (i < p->bufptr) {
						while (i < p->bufptr && p->pipebuf[i] != '\n')
							i++;
						if (p->pipebuf[i] == '\n' ||
						    i > sizeof(p->pipebuf)-2) {
							/* ship out this much */
							int j;
							p->pipebuf[i] = '\0';
							dolog(sv, p, p->pipebuf);
							for (j=i+1 ; j<p->bufptr ; j++)
								p->pipebuf[j-i-1] = p->pipebuf[j];
							p-> bufptr -= i+1;
							i = 0;
						} else
							i++;
					}
				}
			}
			if (p->pipefd >= 0)
				listenon(p->pipefd);
		}
		if (p->exit_time > 0 && p->hold_time < now && p->it_forked == 0
		    && (p->pipefd == -1 || p->exit_time +30 <now)) {
			/* clean up */
			if (p->pipefd>=0) {
				/* it has been 30 seconds since the parent exitted, time to
				 * discard the pipe...
				 */
				close(p->pipefd);
				p->pipefd = -1;
			}
			skip_delete(sv->proc_list, &p->pid);
			skip_delete(allprocs, &p->pid);
			free(p);
		}
		else if (p->exit_time > 0 && p->hold_time > 2)
			waituntil(p->hold_time+1);
	}
	(sv->class->c_check_service)(sv);
}

int new_proc(service_t sv, char **env)
{
	/* fork, register new child
	 * in child, call sv->class->new_child for final setup before exec
	 */
	int pid;
	proc_t p;
	char **envp;
	extern char **environ;
	time_t now;
	int pipefd[2];

	if (sv->enabled == 0)
		return -2;
	if (sv->max_proc > 0 && count_procs(sv) >= sv->max_proc)
		return -2; /* too many already */

	if (sv->class->prefork(sv))
		return -2;

	now = time(0);
	if (sv->watch_output) {
		if (pipe(pipefd)== -1)
			pipefd[0] = pipefd[1] = -1;
	}
	switch (pid = fork()) {
	case -1:
		if (sv->watch_output) {
			close(pipefd[0]); close(pipefd[1]);
		}
		return -1; /* cannot fork */
	case 0: /* child */
		errors_to(ERROR_SYSLOG, NULL);
		if (sv->home_dir) {
			if (chdir(sv->home_dir)!=0) {
				error("Couldn't chdir to %s", sv->home_dir);
				exit(10);
			}
		}
		if (sv->username) {
			struct passwd *pw = getpwnam(sv->username);
			if (pw) {
				initgroups(sv->username, pw->pw_gid);
				setgid(pw->pw_gid);
				setuid(pw->pw_uid);
			} else {
				error("unknown user %s", sv->username);
				exit(10);
			}
		}
		(sv->class->new_child)(sv);
		if (!env)
			envp = environ;
		else {
			int cnt, i;
			cnt = 0;
			for (i=0 ; env[i] ; i++) cnt++;
			for (i=0 ; environ[i] ; i++) cnt++;
			envp = (char**)malloc((cnt+1) * sizeof(char*));
			cnt = 0;
			for (i=0 ; env[i] ; i++)
				envp[cnt++] = env[i];
			for (i=0 ; environ[i] ; i++)
				envp[cnt++] = environ[i];
			envp[cnt] = NULL;
		}
		if (sv->watch_output && pipefd[0] >= 0) {
			close(pipefd[0]);
			if (pipefd[1] != 1) close(1);
			if (pipefd[1] != 2) close(2);
			if (pipefd[1] == 0) {
				pipefd[1] = dup(pipefd[1]);
				close(0);
			}
			open("/dev/null", 0);
			dup(pipefd[1]);
			if (pipefd[1] > 2) {
				dup(pipefd[1]);
				close(pipefd[1]);
			}
		}
		execve(sv->program, sv->args, envp);
		exit(11);
	default: /* parent */
		p = (proc_t)malloc(sizeof(struct proc));
		if (sv->watch_output) {
			close(pipefd[1]);
			p->pipefd = pipefd[0];
			p->bufptr = 0;
			if (p->pipefd >= 0) listenon(p->pipefd);
			fcntl(p->pipefd, F_SETFD, 1);
		}
		else
			p->pipefd = -1;
		p->pid = pid;
		p->service = sv;
		p->it_forked = 0;
		p->is_crash = 0;
		p->start_time = now;
		p->hold_time = 0;
		p->exit_time = 0;
		skip_insert(sv->proc_list, p);
		sv->start_cnt ++;
		skip_insert(allprocs, p);
		sv->class->new_parent(sv,p);
	}
	return pid;
}

void prepare_restart(void)
{
	service_t *sp;
	for (sp = skip_first(services) ; sp ; sp=skip_next(sp)) {
		service_t sv = *sp;
		proc_t *pp;
		for (pp=skip_first(sv->proc_list) ; pp ; pp=skip_next(pp)) {
			proc_t p = *pp;
			if (p->pipefd >= 0)
				fcntl(p->pipefd, F_SETFD, 0);
		}
	}
}

static int proc_cmp(proc_t a, proc_t b, int *kp)
{
	int k;
	if (b) k=b->pid; else k = *kp;
	return k - a->pid;
}

service_t new_service(char *name, class_t class)
{
	service_t sv = (service_t)malloc(sizeof(struct service));

	sv->service = strdup(name);
	sv->class = class;
	sv->max_proc = 1;
	sv->crash_prog = NULL;
	sv->home_dir = NULL;
	sv->username = NULL;
	sv->pidfile = NULL;
	sv->watch_output = 0;
	sv->enabled = 1;
	sv->start_cnt = 0;
	sv->program = NULL;
	sv->args = 0;
	sv->pending = 0;
	sv->proc_list = skip_new(proc_cmp, NULL, NULL);
	sv->next_hold = 2;
	(*class->init_state)(sv);
	return sv;
}

int process_opt(service_t sv, char *opt)
{
	/* max= crash= dir= user= enabled= */

	if (strncmp(opt, "max=", 4)==0)	{
		sv->max_proc = atoi(opt+4);
		return 1;
	} else if (strncmp(opt, "crash=", 6)==0) {
		sv->crash_prog = strdup(opt+6);
		return 1;
	} else if (strncmp(opt, "dir=", 4)==0) {
		sv->home_dir = strdup(opt+4);
		return 1;
	} else if (strncmp(opt, "user=", 5)==0) {
		sv->username = strdup(opt+5);
		return 1;
	} else if (strncmp(opt, "enabled=", 8)==0) {
		if (strcmp(opt+8, "no")==0)
			sv->enabled = 0;
		else if (strcmp(opt+8, "yes")==0)
			sv->enabled = 1;
		else return -1;
		return 1;
	} else if (strncmp(opt, "pidfile=", 8)==0) {
		sv->pidfile = strdup(opt+8);
		return 1;
	} else if (strcmp(opt, "watch_output")==0) {
		sv->watch_output = 1;
		return 1;
	} else
		return 0;
}

static int service_cmp(service_t a, service_t b, char *k)
{
	if (b) k = b->service;
	return strcmp(a->service, k);
}

void service_init(void)
{
	services = skip_new(service_cmp, NULL, NULL);
	allprocs = skip_new(proc_cmp, NULL, NULL);
}

void free_service(service_t sv)
{
	if (sv->service) free(sv->service);
	if (sv->crash_prog) free(sv->crash_prog);
	if (sv->classinfo) (sv->class->free_state)(sv);
	if (sv->home_dir) free(sv->home_dir);
	if (sv->username) free(sv->username);
	if (sv->program) free(sv->program);
	if (sv->pidfile) free(sv->pidfile);
	if (sv->args) strlistfree(sv->args);
	if (sv->proc_list) skip_free(sv->proc_list);

	free(sv);
}

service_t find_service(char *name)
{
	service_t *sp;
	if (name == NULL) return NULL;
	sp = skip_search(services, name);
	if (sp)
		return *sp;
	else
		return NULL;
}
