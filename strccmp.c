#include <string.h>

int strncasecmp(a,b, n)
char *a, *b;
int n;
{
	char ac=0, bc=0;

	while(n--)
	{
		ac = *a++;
		if (ac>='a' && ac <= 'z') ac -= 'a'-'A';
		bc = *b++;
		if (bc>='a' && bc <= 'z') bc -= 'a'-'A';
		if (ac == 0 || ac != bc) break;
	}
	if (ac<bc) return -1;
	if (ac > bc) return 1;
	return 0;
}

int strcasecmp(a,b)
char *a, *b;
{
	return strncasecmp(a,b,(int)strlen(a)+1);
}
