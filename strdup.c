
#define NULL (0)
char *malloc();

char *strdup(char *s)
{
	if (s==NULL)
		return s;
	return (char *)strcpy(malloc(strlen(s)+1), s);
}

char *strndup(char *s, int a)
{
	char *r;
	if (s == NULL)
		return s;

	r = (char*)malloc(a+1);
	strncpy(r, s, a);
	r[a] = 0;
	return r;
}
