
#include	"metad.h"
#include	<netdb.h>
#include	<sys/socket.h>
#include	<netinet/in.h>
#include	<fcntl.h>
#ifdef SOLARIS
#include	<sys/fcntl.h>
#else
#include	<sys/file.h>
#endif

typedef struct stream_opts
{
	int	proto;	/* always TCP */
	int	port;
	int	backlog;
	int	sock;
	int	newsock;
} *stream_t;

#define c(sv) ((stream_t)((sv)->classinfo))

static int stream_opt(service_t sv, char *opt)
{
	/* port= */
	char *protos = "tcp";
	if (strncmp(opt, "port=", 5) == 0) {
		char *ep;
		int port;
		struct servent *se;
		port = strtoul(opt+5, &ep, 10);
		if (opt[5] && !*ep) {
			c(sv)->port = htons(port);
			return 1;
		}
		se = getservbyname(opt+5, protos);
		if (se) {
			c(sv)->port = se->s_port;
			return 1;
		}
	}
	if (strncmp(opt, "backlog=", 8) == 0) {
		char *ep;
		c(sv)->backlog = strtoul(opt+8, &ep, 10);
		if (opt[8] && !*ep)
			return 1;
	}
	return 0;
}

static void stream_register(service_t sv)
{
	/* create a socket and bind it */
	stream_t s = sv->classinfo;
	struct sockaddr_in addr;
	int opt;

	if (s->sock >=0 || s->port <= 0)
		return;
	s->sock = socket(AF_INET, SOCK_STREAM, s->proto);
	if (s->sock < 0)
		return;
	opt = 1;
	setsockopt(s->sock, SOL_SOCKET, SO_REUSEADDR, (char*)&opt, sizeof(opt));

	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_port = s->port;
	if (bind(s->sock, (struct sockaddr *)&addr, sizeof(addr))) {
		close(s->sock);
		s->sock = -1;
		return;
	}

	if (listen(s->sock, s->backlog)<0) {
		close(s->sock);
		s->sock = -1;
		return;
	}
	nodelay(s->sock);
}

static void stream_unregister(service_t sv)
{
	stream_t s = sv->classinfo;
	if (s->sock>=0)
		close(s->sock);
	s->sock = -1;
}

static void stream_check(service_t sv)
{
	stream_t s = sv->classinfo;

	if (s->sock >=0) {
		if (readyon(s->sock)) {
			char *env[3];
			env[0] = "METAD_REASON=connect";
			env[1] = "METAD_ARG=";
			env[2] = NULL;
			if (new_proc(sv, env) == -1)
				if (s->newsock >= 0) {
					close(s->newsock);
					s->newsock = -1;
				}
		}
		listenon(s->sock);
	}
}

static int stream_prefork(service_t sv)
{
	int f;
	stream_t s = sv->classinfo;
	if (s->sock < 0)
		return -2;

	s->newsock = accept(s->sock, NULL, 0);
	if (s->newsock < 0)
		return -2;
	/* disable NDELAY which might be inherited */
	f = fcntl(s->newsock, F_GETFL, 0);
	f &= ~O_NDELAY;
	fcntl(s->newsock, F_SETFL, f);
	return 0;
}

static void stream_init(service_t to)
{
	/* set up defaults */
	stream_t n;
	n = (stream_t)malloc(sizeof(struct stream_opts));

	n->proto = IPPROTO_TCP;
	n->backlog = 10;
	n->port = 0;
	n->sock = -1;
	n->newsock = -1;

	to->classinfo = n;
}

static void stream_copy(service_t from, service_t to)
{
	/* copy state */

	c(to)->sock = c(from)->sock;
	c(from)->sock = -1;
}

static void stream_freestate(service_t sv)
{
	stream_t s = sv->classinfo;
	if (s->sock >= 0)
		close(s->sock);
	free(s);
}

static void stream_newparent(service_t sv, proc_t p)
{
	if (c(sv)->newsock >= 0) {
		close(c(sv)->newsock);
		c(sv)->newsock = -1;
	}
}

static void stream_newchild(service_t sv)
{
	close(c(sv)->sock);
	if (c(sv)->newsock < 0)
		exit(2);
	dup2(c(sv)->newsock, 0);
	dup2(c(sv)->newsock, 1);
	if (c(sv)->newsock > 1)
		close(c(sv)->newsock);

}

static void stream_send(service_t sv)
{
	send_byte(2);	/*stream */
	send_int(c(sv)->proto);
	send_int(ntohs(c(sv)->port));
	send_int(c(sv)->backlog);
	send_int(c(sv)->sock>=0);
}

struct class stream_class = {
	.class		= "stream",
	.c_process_opt	= stream_opt,
	.register_service= stream_register,
	.c_check_service= stream_check,
	.init_state	= stream_init,
	.copy_state	= stream_copy,
	.free_state	= stream_freestate,
	.send_class	= stream_send,
	.disable_service= stream_unregister,
	.new_parent	= stream_newparent,
	.new_child	= stream_newchild,
	.prefork	= stream_prefork,
};

