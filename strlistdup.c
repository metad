
/* duplicate a list of strings */

#include <malloc.h>
#include <string.h>

char **strlistdup(char **l)
{
	int len = 0;
	char **rv;
	while (l[len])
		len++;

	rv = (char**)malloc((len+1)*sizeof(char *));
	for (len=0 ; l[len]; len++)
		rv[len] = strdup(l[len]);
	rv[len] = NULL;
	return rv;
}

void strlistfree(char **l)
{
	int i;
	for (i=0 ; l[i] ; i++)
		free(l[i]);
	free(l);
}

