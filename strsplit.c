#include <malloc.h>
#include <string.h>
#include <stdio.h>

/*
 * split a string into an array of strings,
 * separated by any chars in the fs string
 * strings may contain chars from fs if quoted
 * quotes may be escaped or quoted
 *
 * MWR, UNSW, Oct 84
 */

char **
strsplit(char *s, char *fs)
{
	char		*sp, *sp2, *delim, **ssp, *ns;
	unsigned	i, num;
	static char		quote[] = "'";

	if((ns = malloc((unsigned) strlen(s) + 1)) == NULL)
		return NULL;
	sp = s;
	sp2 = ns;
	num = 0;
	while(*sp)
	{
		while(*sp && strchr(fs, *sp))
			sp++;
		if(!*sp)
			break;

		num++;
		do
		{
			delim = fs;
			while(*sp && !strchr(delim, *sp))
			{
				switch(*sp)
				{
				case '\'':
				case '"':
					if(delim == fs)
					{
						quote[0] = *sp++;
						delim = quote;
						continue;
					}
					break;
				case '\\':
					if(sp[1] == '\'' || sp[1] == '"')
						sp++;
					break;
				}
				*sp2++ = *sp++;
			}
			if(delim == quote && *sp)
				sp++;
		}
		while(delim == quote && *sp);
		*sp2++ = '\0';
	}

	if((ssp = (char **) malloc(sizeof(char *) * (num + 2))) == NULL)
		return NULL;
	sp = ns;
	ssp[0] = ns;
	for(i = 1; i <= num; i++)
	{
		ssp[i] = sp;
		while(*sp)
			sp++;
		sp++;
	}
	ssp[i] = NULL;
	return &ssp[1];
}
