
char version[] = "2.17";

/*
 * 2.17 06aug2003 metad: check if pid has been collected before assuming a failed kill means that it is lost.
 * 2.16 18sep2002 metad: add "stream" service for tcp listening
 *		  metac: understand listing of stream service
 * 2.15 17aug2001 metad: fixed ordering in main_loop
 * 2.14 07aug2001 metad: short malloc in new_proc was causing problems
 * 2.13 30apr99   metad: errors during startup were getting lost
 *			 lookup username every time instead of once only
 *			 disassociate from controlling tty
 * 2.12 25may98   metad: bugfix - pidfile was not being closed!!
 * 2.11 08oct96   metad: bug whereby would sometimes spin on failing process as next_hold was set to 0
 * 2.10 15aug96   metad: that last fix was broken - try again
 * 2.09 02aug96   metad: where re-read config file, didn't update server pointers in proc structures...
 * 2.08 04jul96   metad: if pidfile and watchout_put options in force, then
 *		      metad would wait for pipe to close even if daemon exitted.
 *		      Now metad only wait 30 more seconds for pipe to close, then closes it.
 *		      Also restart properly saves it_forked info.
 * 2.07 02apr96	  metad: fix bug in user= processing
 * 2.06 06mar96   metad: fix memory buf in free_service
 * 2.05 01feb96   metad: preserve enabled state over restart
 * 2.04 04dec95   metad: wait a little while for child to write to pidfile -- xdm is slow
 * 2.03 14nov95   metad: fixed bug where messaged returned on udb request caused crash
 * 2.02 31oct95   metad: pidfile and watch_output options, minor improvements to logging
 *       	  metac: understand new protocol phrases for pidfile and watch_output
 * 2.01 25oct95   metad: zero holdtimes when run command received
 *		  metac: improve listings: brief and long, hostname included
 *
 */
